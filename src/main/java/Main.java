import model.Animal;
import model.Gun;
import model.Hunter;
import model.arsenal.Crossbow;
import model.arsenal.RemingtonRifle;
import model.zoo.Bear;
import model.zoo.Dinosaur;

public class Main {

  public static void main(String[] args) {
    Hunter elmer = new Hunter();
    Gun gun = new RemingtonRifle("750", 45);
    Animal yogui = new Bear(10, 1500, 300);

    elmer.shoot(yogui, gun);
    System.out.println(yogui.isAlive() ? "Run Yogi!" : "Oh no, poor Yogi");

    Gun crossbow = new Crossbow();
    Animal dino = new Dinosaur();
    elmer.shoot(dino, crossbow);
    System.out.println(yogui.isAlive() ? "Run Elmer!!!" : "Man, what a powerful crossbow!");
  }
}

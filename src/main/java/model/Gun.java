package model;

import java.util.Objects;

public abstract class Gun {

  private static final float MAX_DAMAGE = 1000;
  private static final float DEFAULT_CALIBER = 9;

  private final String model;
  private final float caliber;
  private final float damage;

  public Gun(String model, float caliber, float damage) {
    this.model = model;
    this.caliber = caliber;
    this.damage = maxDamage(damage);
  }

  public Gun(String model, float damage) {
    this.model = model;
    this.caliber = DEFAULT_CALIBER;
    this.damage = maxDamage(damage);
  }

  public float shoot(Animal animal) {
    if (Objects.nonNull(animal)) {
      return animal.takeShoot(damage);
    } else {
      throw new RuntimeException(
          "What are you trying to do, you can't shoot to an imaginary animal");
    }
  }

  private float maxDamage(float damage) {
    return Math.min(damage, MAX_DAMAGE);
  }

  public String getModel() {
    return this.model;
  }

  public float getCaliber() {
    return this.caliber;
  }

  public float getDamage() {
    return damage;
  }
}

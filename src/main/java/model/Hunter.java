package model;

import model.arsenal.Slingshot;

import java.util.Objects;

public class Hunter {

  private final String name;
  private Gun gun;

  public Hunter() {
    this.name = "Elmer";
    this.gun = new Slingshot();
  }

  public Hunter(String name) {
    this.name = name;
    this.gun = new Slingshot();
  }

  public Hunter(String name, Gun gun) {
    this.name = name;
    this.gun = gun;
  }

  public float shoot(Animal animal, Gun gun) {
    this.setGun(gun);
    return this.gun.shoot(animal);
  }

  public float shoot(Animal animal) {
    return this.gun.shoot(animal);
  }

  public String getName() {
    return name;
  }

  public Gun getGun() {
    return this.gun;
  }

  public void setGun(Gun gun) {
    if(Objects.nonNull(gun)) this.gun = gun;
  }
}

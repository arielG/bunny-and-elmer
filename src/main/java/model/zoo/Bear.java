package model.zoo;

import model.Animal;

public class Bear extends Animal {

  private final static float AVERAGE_LIFE = 120;

  public Bear(String name, float age, float weight, float height) {
    super(name, age, weight, height, AVERAGE_LIFE);
  }

  public Bear(float age, float weight, float height) {
    super("Yogi", age, weight, height, AVERAGE_LIFE);
  }
}

package model.zoo;

import model.Animal;

public class Demogorgon extends Animal {

  private static final float AVERAGE_LIFE = 500000000000000f;

  public Demogorgon(String name, float age, float weight, float height) {
    super(name, age, weight, height, AVERAGE_LIFE);
  }

  public Demogorgon(String name, float age, float weight, float height, float health) {
    super(name, age, weight, height, health);
  }

  public Demogorgon(float age, float weight, float height) {
    super("Demogorgon", age, weight, height, AVERAGE_LIFE);
  }
}

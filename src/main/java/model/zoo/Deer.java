package model.zoo;

import model.Animal;

public class Deer extends Animal {

  private static final float AVERAGE_LIFE = 40;

  public Deer(String name, float age, float weight, float height) {
    super(name, age, weight, height, AVERAGE_LIFE);
  }

  public Deer(float age, float weight, float height) {
    super("Bamby", age, weight, height, AVERAGE_LIFE);
  }
}

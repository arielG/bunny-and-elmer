package model.zoo;

import model.Animal;

public class Dinosaur extends Animal {

  private static final String DEFAULT_NAME = "Barney";
  private static final float DEFAULT_AGE = 100;
  private static final float DEFAULT_WEIGHT = 4000;
  private static final float DEFAULT_HEIGHT = 30;
  private static final float AVERAGE_HEALTH = 500;

  public Dinosaur() {
    super(DEFAULT_NAME, DEFAULT_AGE, DEFAULT_WEIGHT, DEFAULT_HEIGHT, AVERAGE_HEALTH);
  }

  public Dinosaur(String name, float age, float weight, float height, float health) {
    super(name, age, weight, height, health);
  }
}

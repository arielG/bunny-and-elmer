package model;

public abstract class Animal {

  private static final float MAX_HEALTH = 10000;

  protected String name;
  protected float age;
  protected float weight;
  protected float height;
  private float health;

  protected Animal(String name, float age, float weight, float height, float health) {
    this.name = name;
    this.age = age;
    this.weight = weight;
    this.height = height;
    this.health = maxHealth(health);
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public float getAge() {
    return age;
  }

  public float getWeight() {
    return weight;
  }

  public float getHeight() {
    return height;
  }

  public float getHealth() {
    return health;
  }

  public boolean isAlive() {
    return this.health > 0;
  }

  public float takeShoot(float damage) {
    if (this.health - damage > 0) {
      this.health -= damage;
      return damage;
    } else {
      this.health = 0;
      return health;
    }
  }

  private float maxHealth(float health) {
    return Math.min(health, MAX_HEALTH);
  }
}

package model.arsenal;

import model.Gun;

public class Crossbow extends Gun {

  private static final String DEFAULT_NAME = "A good one";
  private static final float DEFAULT_DAMAGE = 15f;
  private static final float DEFAULT_CALIBER = 10;

  public Crossbow() {
    super(DEFAULT_NAME, DEFAULT_CALIBER, DEFAULT_DAMAGE);
  }

  public Crossbow(String model, float caliber) {
    super(model, caliber, DEFAULT_DAMAGE);
  }

}

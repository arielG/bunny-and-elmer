package model.arsenal;

import model.Gun;

public class RemingtonRifle extends Gun {

  private static final float DEFAULT_DAMAGE = 50;

  public RemingtonRifle(String model, float caliber) {
    super(model, caliber, DEFAULT_DAMAGE);
  }
}

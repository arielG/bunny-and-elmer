package model.arsenal;

import model.Gun;

public class Colt extends Gun {

  private static final float DEFAULT_DAMAGE = 15f;

  public Colt(String model, float caliber) {
    super(model, caliber, DEFAULT_DAMAGE);
  }
}

package model.arsenal;

import model.Gun;

public class Agneyastra extends Gun {

    private static final String DEFAULT_MODEL = "The only one";

    public Agneyastra(float damage) {
        super(DEFAULT_MODEL, damage);
    }
}

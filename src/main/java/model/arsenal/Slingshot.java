package model.arsenal;

import model.Gun;

public class Slingshot extends Gun {

  private static final String DEFAULT_MODEL = "Mine";
  private static final float DEFAULT_CALIBER = 1;
  private static final float DEFAULT_DAMAGE = 5f;

  public Slingshot() {
    super(DEFAULT_MODEL, DEFAULT_CALIBER, DEFAULT_DAMAGE);
  }
}

import model.Animal;
import model.Gun;
import model.Hunter;
import model.arsenal.Agneyastra;
import model.arsenal.Colt;
import model.arsenal.Crossbow;
import model.arsenal.RemingtonRifle;
import model.arsenal.Slingshot;
import model.zoo.Deer;
import model.zoo.Demogorgon;
import model.zoo.Dinosaur;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("The Jungle rules")
public class TheJungleLab {

  @Test
  @DisplayName("When Elmer is carrying a remington 45, and finds a Bamby then, poor baby :(")
  void ElmerIsCarryingARemington45AndBabyIsAround() {
    Hunter elmer = new Hunter("Elmer");
    Gun remingtonRifle = new RemingtonRifle("750", 45f);
    Animal bamby = new Deer(3f, 10f, 20f);

    elmer.shoot(bamby, remingtonRifle);

    assertFalse(bamby.isAlive());
    assertEquals(0, bamby.getHealth());
  }

  @Test
  @DisplayName("When Elmer is carrying a remington 45, and finds a Bamby then, is alive!!")
  void ElmerIsCarryingAColtAndBabyIsAround() {
    Hunter elmer = new Hunter("Elmer");
    Gun coltDragoon = new Colt("Dragoon", 22f);
    Animal bamby = new Deer(3f, 10f, 20f);

    elmer.shoot(bamby, coltDragoon);

    assertTrue(bamby.isAlive());
    assertEquals(25, bamby.getHealth());
  }

  @Test
  @DisplayName(
      "When Elmer is trying to build an infinite damage gun and finds an 'inmortal' animal, then, yeah sure... :|")
  void ElmerCantBuildAnInfalibleGunAndTheInmortalsDontExists() {
    Hunter elmer = new Hunter();
    Gun agneyastra = new Agneyastra(Float.MAX_VALUE);
    Animal demogorgon = new Demogorgon(10f, 20f, Float.MAX_VALUE);

    elmer.shoot(demogorgon, agneyastra);

    assertTrue(demogorgon.isAlive());
    assertEquals(9000, demogorgon.getHealth());
  }

  @Test
  @DisplayName(
      "When Elmer have an nonexistent gun and finds an animal, then at least should to shoot with the slingshot")
  void ElmerHaveANullGunShouldUseTheSlingshot() {
    Hunter elmer = new Hunter();
    Gun nonexistentGun = null;
    Animal nonexistentAnimal = null;

    assertThrows(
        RuntimeException.class,
        () -> elmer.shoot(nonexistentAnimal, nonexistentGun),
        "What are you trying to do, you can't shoot to an imaginary animal");
    assertInstanceOf(Slingshot.class, elmer.getGun());
  }

  @Test
  @DisplayName(
      "When Elmer is testing the arsenal" + "then, at least should to shoot with the slingshot")
  void ElmerIsTestingTheArsenal() {

    Hunter elmer = new Hunter();
    Animal mockDino = new Dinosaur();

    final var damage1 = elmer.shoot(mockDino);
    assertInstanceOf(Slingshot.class, elmer.getGun());
    assertEquals(elmer.getGun().getDamage(), damage1);

    Gun crossbow = new Crossbow();
    elmer.setGun(crossbow);
    final var damage2 = elmer.shoot(mockDino);
    assertInstanceOf(Crossbow.class, elmer.getGun());
    assertEquals(elmer.getGun().getDamage(), damage2);

    Gun rifle = new RemingtonRifle("750", 45);
    elmer.setGun(rifle);
    final var damage3 = elmer.shoot(mockDino);
    assertInstanceOf(RemingtonRifle.class, elmer.getGun());
    assertEquals(elmer.getGun().getDamage(), damage3);

    assertTrue(mockDino.isAlive());
  }
}

# BUGS BUNNY y EL CAZADOR

Enfocado 100% en lograr un modelo orientado a objetos lo más elegante posible, este ejercicio solicita programar una
simulación de la siguiente situación que cumpla con los requerimientos indicados (puede ejecutarse directo en consola,
no hagas una API ni una UI). Las reglas del juego son la siguientes:

* Tu modelo debe tener el comportamiento solicitado.
* El evaluador de tu modelo podrá instanciar tu modelo en el main y configurarlo para que funcione.
* Debes tratar de que el evaluador no pueda violar el comportamiento deseado (hacer trampa) sea por como setea la
  simulacion o porque implementa una nueva clase que permite violar el comportamiento.
* El evaluador no tocará tu código ni modificará tu modelo pero si el problema lo especifica debería poder implementar
  nuevas clases en base a tu modelo para que sean utilizadas en la simulación.

**Situación:**

El cazador Elmer está buscando a Bugs Bunny en el bosque para cazarlo y lleva una escopeta, por ejemplo una escopeta
Remington modelo 750 con balas calibre 45. Elmer le dispara a un venado. Para un venado, un disparo de calibre 45 es
mortal. Si el cazador estuviese utilizando una pistola Colt con balas calibre 22 en vez de la escopeta con balas calibre
45, el venado podría sobrevivir a 1 disparo, pero no a 2.

Elmer sigue buscando a Bugs Bunny, pero si se encontrase con un oso (el Oso Yogui), necesitaría dispararle por lo menos
3 veces con la escopeta, mientras que se calcula que si le disparase con la pistola calibre 22, debería asestarle por lo
menos 20 disparos para cazarlo. Por suerte Elmer no encontró a Bugs, ya que el impacto de cualquier arma en él sería
mortal.

Por la descripción se puede apreciar que no todos los calibres tienen el mismo efecto en las diferentes presas de Elmer.
Tu modelo debería poder implementar nuevos tipos de animales y nuevos tipos de armas de fuego que Elmer (un cazador)
podría utilizar pero no debería ser posible ni implementar armas que maten cualquier cosa ni animales que sean
invencibles.

*Nota: El evaluador deberá instanciar un cazador, darle un arma de un tipo específico para que le dispare a la presa que
se le indique capturar.*

```java
public void main(String args[]){

        Hunter elmer=new Hunter();
        Gun gun=new RemingtonRifle(750,45);
        Animal yogi=new Bear(10,1500,300);

        elmer.shoot(yogi,gun);
        System.out.println(yogui.isAlive()?"Run Yogi!":"Oh no, poor Yogi");

        Gun crossbow=new Corssbow();
        Animal dino=new Dinosaur();
        elmer.shoot(dino,crossbow);
        System.out.println(yogui.isAlive()?"Run Elmer!!!":"Man, what a powerful crossbow!");

}
```

## Ejecutable

```
$> ./gradlew clean build
$> java -jar build/libs/*.jar